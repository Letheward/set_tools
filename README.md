# [Set Tools for NoCS](https://letheward.codeberg.page/set_tools/)

## How To Use

Left: Enter set, transform them, and play audio to get how they sound.

Right: Browse all possible subsets of the chromatic scale, filter and sort them. You can click on a set to copy to the left set input.

## Status

It's basically usable, but still needs some UI/UX polishing. Some features are not done yet. 

Features that may not seems obvious (will improve this later):

Left:
- `Play`: If no speed is given, it will play the set at speed of `4`. Also, you can hit `spacebar` to play the set anywhere in the page.

Right:
- `Neutral`, `Sparse`, `Pure Tertian`, `by Size`, `by UIV`, `by PV`: filter only sets that are shown **in the view**, so you can combine them.
- `Sort` dropdown: sort sets that are shown **in the view**.
- `by Size`, `by PV`, `by UIV`, `Custom PV Weighting`: press `Enter` to confirm.

## License

Except for all font assets, this repository is dedicated to public domain, or under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
