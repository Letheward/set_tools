# Notes

## Bugs

- old Edge will not load the page correctly (unless we open console and reload?????)
- old Edge will not give us equal width in the left section

## TODO

- make filters UI more intuitive
- toggle-style filters UI? make "All Sets" different from others (because it's a reset)? 
- shortcuts?
- make it fast (set filters will trigger very slow page layout reflow)
- further code cleanup

