/* ==== Boilerplate ==== */

"use strict"

const print  = console.log
const assert = console.assert
const error  = (t) => print("[Error]", t)

const array_copy = (input) => {
    let out = []
    for (let i = 0; i < input.length; i++) out[i] = input[i]
    return out
}

const mod = (x, m) => ((x % m) + m) % m  // get around bad javascript mod behaviour
const gcd = (x, y) => {
    
    x = Math.abs(x)
    y = Math.abs(y)
    
    while (y) {
        let t = y
        y = x % y
        x = t
    }

    return x
}

const get_node      = (id ) => document.getElementById(id)
const make_node     = (tag) => document.createElement(tag)
const make_svg_node = (tag) => document.createElementNS("http://www.w3.org/2000/svg", tag)




/* ==== Set Info ==== */

const SetInfo = () => ({
    data:                      [], 
    index:                     0,
    polarity_value:            0,                
    interval_vector_ordered:   [],
    interval_vector_unordered: [],
})


/* ---- Set Info: Helpers ---- */

//  0 6   7 2 9 4 11   5 10 3 8  1
//  to
//  0 1   2 3 4 5 6    7 8  9 10 11
const translate_weighting_IC7_order_to_index_order = (weighting) => {
    
    assert(weighting.length >= 12)

    const copy = []
    for (let i = 0; i < 12; i++) copy[i] = weighting[i] 
    
    weighting[0 ]  = copy[0]
    weighting[6 ]  = copy[1]
    weighting[7 ]  = copy[2]
    weighting[2 ]  = copy[3]
    weighting[9 ]  = copy[4]
    weighting[4 ]  = copy[5]
    weighting[11]  = copy[6]
    weighting[5 ]  = copy[7]
    weighting[10]  = copy[8]
    weighting[3 ]  = copy[9]
    weighting[8 ]  = copy[10]
    weighting[1 ]  = copy[11]

    return weighting
}


//  1 2 3 4  5  6  7 8  9 10 11
//  to
//  7 2 9 4 11  6  5 10 3 8  1
const translate_OIV_index_order_to_IC7_order = (input) => {

    assert(input.length >= 11)
    
    const copy = []
    for (let i = 0; i < 11; i++) copy[i] = input[i]
    
    input[0]  = copy[6]
    input[1]  = copy[1]
    input[2]  = copy[8]
    input[3]  = copy[3]
    input[4]  = copy[10]
    input[5]  = copy[5]
    input[6]  = copy[4]
    input[7]  = copy[9]
    input[8]  = copy[2]
    input[9]  = copy[7]
    input[10] = copy[0]

    return input
}


const Compares = {}
{
    const C = Compares
    
    C.index_ascend = (a, b) => {
        let av = a.index
        let bv = b.index
        if (av > bv) return  1
        if (av < bv) return -1
        return  0
    }
   
    C.PV_descend = (a, b) => {
        let av = a.polarity_value
        let bv = b.polarity_value
        if (av < bv) return  1
        if (av > bv) return -1
        return  0
    }

    C.count_ascend = (a, b) => {
        let av = a.data.length
        let bv = b.data.length
        if (av > bv) return  1
        if (av < bv) return -1
        return  0
    }

    C.count_ascend_then_value_descend = (a, b) => {
        let count = C.count_ascend( a, b)
        let value = C.PV_descend(a, b)
        if (count !== 0) return count
        return value
    }
    
    C.UIV_descend = (a, b) => {
        
        let av = a.interval_vector_unordered
        let bv = b.interval_vector_unordered
        
        for (let i = 0; i < 6; i++) {
            let ac = av[i]
            let bc = bv[i]
            if (ac < bc) return  1
            if (ac > bc) return -1
        }

        return 0
    }

    C.UIV_ascend = (a, b) => {
        
        let av = a.interval_vector_unordered
        let bv = b.interval_vector_unordered
        
        for (let i = 0; i < 6; i++) {
            let ac = av[i]
            let bc = bv[i]
            if (ac > bc) return  1
            if (ac < bc) return -1
        }

        return 0
    }


    C.OIV_descend = (a, b) => {
        
        let av = a.interval_vector_ordered
        let bv = b.interval_vector_ordered
        
        for (let i = 0; i < 11; i++) {
            let ac = av[i]
            let bc = bv[i]
            if (ac < bc) return  1
            if (ac > bc) return -1
        }

        return 0
    }

    C.OIV_ascend = (a, b) => {
        
        let av = a.interval_vector_ordered
        let bv = b.interval_vector_ordered
        
        for (let i = 0; i < 11; i++) {
            let ac = av[i]
            let bc = bv[i]
            if (ac > bc) return  1
            if (ac < bc) return -1
        }

        return 0
    }


    C.UIV_OIV_descend = (a, b) => {
        let UIV = C.UIV_descend(a, b)
        let OIV = C.OIV_descend(a, b)
        if (UIV !== 0) return UIV
        return OIV
    }

    C.OIV_UIV_descend = (a, b) => {
        let OIV = C.OIV_descend(a, b)
        let UIV = C.UIV_descend(a, b)
        if (OIV !== 0) return OIV
        return UIV
    }

    C.count_ascend_UIV_OIV_descend = (a, b) => {

        let count = C.count_ascend(a, b)
        let UIV   = C.UIV_descend( a, b)
        let OIV   = C.OIV_descend( a, b)

        if (count !== 0) return count
        if (UIV   !== 0) return UIV
        return OIV
    }
}




/* ---- Set Info: Generators and Filters ---- */

const get_all_set_info = () => {
    
    const sets = []

    const done = 1 << 11 
    let binary = 0 

    while (binary < done) {

        let p = []
        p[0] = 0
        
        let c = 1
        for (let i = 0; i < 11; i++) {
            if (binary & (1 << i)) { // unpack bits 
                p[c] = i + 1
                c++
            }
        }

        sets[binary] = SetInfo()
        sets[binary].data  = p
        sets[binary].index = binary

        binary++
    }
    
    return sets
}

// weighting's length must be 12 or more
const fill_polarity_values = (sets, weighting) => {
    
    assert(weighting.length >= 12)
    
    for (let i = 0; i < sets.length; i++) {
        const p = sets[i]
        p.polarity_value = 0
        for (let i = 0; i < p.data.length; i++) {
            p.polarity_value += weighting[p.data[i]]
        }
    }
}

const fill_interval_vectors = (sets) => {
    
    for (let i = 0; i < sets.length; i++) {

        const set = sets[i]

        for (let i = 0; i < 11; i++) set.interval_vector_ordered  [i] = 0
        for (let i = 0; i < 6;  i++) set.interval_vector_unordered[i] = 0

        for (let j = 0; j < set.data.length; j++) {
            for (let k = j + 1; k < set.data.length; k++) {
                let           index = set.data[k] - set.data[j]
                let unordered_index = index > 6 ? 12 - index : index
                
                set.interval_vector_ordered  [          index - 1] += 1
                set.interval_vector_unordered[unordered_index - 1] += 1
            }
        }
    }
}


// todo: since we know how sets are generated, maybe there is a much faster way to do this
const find_set = (set_infos, set) => {

    const match = (a, b) => {
        if (a.length !== b.length) return false
        for (let i = 0; i < a.length; i++) {
            if (a[i] !== b[i]) return false
        }
        return true
    }

    return set_infos.find(s => match(s.data, set))
}



const Set_Filters = {}
{
    const F = Set_Filters
    
    // lambda generators, maybe not necessary, and we can inline these at call site
    F.by_size = (size)  => (set) => set.data.length === size 
    F.by_PV   = (value) => (set) => set.polarity_value === value
    

    // be careful with iv length, UIV needs to be 6, OIV needs to be 11
    // we can add checks, but will that be slow?

    F.by_UIV = (iv) => (set) => {
        const siv = set.interval_vector_unordered
        for (let i = 0; i < siv.length; i++) {
            if (iv[i] !== siv[i]) return false
        }
        return true
    }

    F.by_OIV = (iv) => (set) => {
        const siv = set.interval_vector_ordered
        for (let i = 0; i < siv.length; i++) {
            if (iv[i] !== siv[i]) return false
        }
        return true
    }


    // lambdas
    F.is_sparse = (set) => {

        const c = set.data.length
        if (c < 2) return true

        let d0 = set.data[    1] - set.data[0]
        let d1 = set.data[c - 1] - set.data[c - 2]
        let d2 = 12 - (set.data[c - 1] - set.data[0])
        
        if ((d0 < 2 && d2 < 2) + (d1 < 2 && d2 < 2)) return false

        for (let j = 0; j < c - 2; j++) {
            let d1 = set.data[j + 1] - set.data[j    ]
            let d2 = set.data[j + 2] - set.data[j + 1]
            if (d1 < 2 && d2 < 2) return false
        }

        return true
    }

    // todo: handle non-generated sets, cleanup and refactor
    F.is_pure_tertian = (set) => {

        const count = set.data.length
        if (count % 2 === 0) return false

        const temp = [12]

        for (let j = 0; j < count; j++) {
            let k = j * 2 % count
            temp[j] = set.data[k] 
        }

        // check pure
        {
            for (let j = 0; j < count - 1; j++) {
                let diff = temp[j + 1] - temp[j]
                if (diff < 0) diff += 12
                if (!(diff === 3 || diff === 4)) return false
            }

            let diff = 12 - temp[count - 1]
            if (!(diff === 3 || diff === 4)) return false
        }
        
        return true
    }

    F.is_neutral = (set) => {

        const s     = set.data
        const count = s.length

        for (let i = 1; i < count; i++) {
            if (s[i] !== (12 - s[count - i])) return false
        }
        
        return true
    }
}



/* ==== User level set info API ==== */

// note: this use full index of 4096 
const convert_set_to_index = (set) => {
    let out = 0
    for (let i = 0; i < set.length; i++) {
        out |= 1 << set[i]
    }
    return out
}

// note: this use full index of 4096 
const convert_index_to_set = (index) => {
    let out = []
    for (let i = 0; i < 12; i++) {
        if (index & (1 << i)) out.push(i)
    }
    return out
}

const apply_binary_operation = (a, b, op) => {

    let ia = convert_set_to_index(a)
    let ib = convert_set_to_index(b)

    let ic = eval("mod(ia " + op + " ib, 4096)") // todo: without integer arithmetics, "/" is not working too well
    
    return convert_index_to_set(ic)
}


const get_PV_from_weighting = (set, weighting) => {

    if (weighting.length < 12) {
        error("Weighting's length must be 12")
        return 
    }
   
    let value = 0
    for (let j = 0; j < set.length; j++) {
        value += weighting[set[j]]
    }
    
    return value
}

const get_OIV = (set) => {

    let iv = []
    for (let i = 0; i < 11; i++) iv[i] = 0

    for (let i = 0; i < set.length; i++) {
        for (let j = i + 1; j < set.length; j++) {
            
            let index = set[j] - set[i]
            if (index <= 0 || index > 11) {
                error("You pass a wrong set to get_OIV(), it must be monotonic and is in one octave.")
                return
            }
            
            iv[index - 1] += 1
        }
    }
    
    return iv
}

const get_UIV = (set) => {

    let iv = []
    for (let i = 0; i < 6; i++) iv[i] = 0

    for (let i = 0; i < set.length; i++) {
        for (let j = i + 1; j < set.length; j++) {
            let index = set[j] - set[i]
            if (index > 6) index = 12 - index

            if (index <= 0 || index > 6) {
                error("You pass a wrong set to get_UIV(), it must be monotonic and is in one octave.")
                return
            }
           
            iv[index - 1] += 1
        }
    }
    
    return iv
}

// must have the info from same weighting first, weighting's length must be 12 or more
const get_PV_count_table = (sets, weighting) => {

    if (weighting.length < 12) {
        error("Weighting's length must be 12")
        return 
    }

    let upper = 0   
    let lower = 0    

    for (let i = 0; i < 12; i++) {
        let v = weighting[i]
        if (v > 0) upper += v
        if (v < 0) lower += v
    }
    
    let range = upper - lower + 1
    
    let table = []
    for (let i = 0; i < range; i++) table[i] = {value: 0, count: 0}

    for (let i = 0; i < sets.length; i++) {
        let value = sets[i].polarity_value
        table[value - lower].count++
    }

    for (let i = range - 1; i >= 0; i--) {
        table[i].value = i + lower
    }

    return table
}


// must have the info first
const get_OIV_count_table = (sets) => {
    
    sets.sort(Compares.OIV_descend)
    
    const iv_count = 11
    
    let table = []
    
    let count = 1

    for (let i = 1; i < sets.length; i++) {

        const prev = sets[i - 1].interval_vector_ordered
        const now  = sets[i    ].interval_vector_ordered
        
        let same = 1

        for (let i = 0; i < iv_count; i++) {
            if (now[i] != prev[i]) {
                same = 0
                break
            }
        }
        
        if (same) {
            count++
            continue
        } else {
            
            let iv = []
            for (let i = 0; i < iv_count; i++) iv[i] = prev[i]
            table.push({OIV: iv, count: count})
            
            count = 1
        }

    }
    
    const prev = sets[sets.length - 1].interval_vector_ordered
    let iv = []
    for (let i = 0; i < iv_count; i++) iv[i] = prev[i]
    table.push({OIV: iv, count: count})
    
    return table
}


// must have the info first
const get_UIV_count_table = (sets) => {
    
    sets.sort(Compares.UIV_descend)
    
    const iv_count = 6
    
    let table = []
    
    let count = 1

    for (let i = 1; i < sets.length; i++) {

        const prev = sets[i - 1].interval_vector_unordered
        const now  = sets[i    ].interval_vector_unordered
        
        let same = 1

        for (let i = 0; i < iv_count; i++) {
            if (now[i] != prev[i]) {
                same = 0
                break
            }
        }
        
        if (same) {
            count++
            continue
        } else {
            
            let iv = []
            for (let i = 0; i < iv_count; i++) iv[i] = prev[i]
            table.push({OIV: iv, count: count})
            
            count = 1
        }
    }
    
    const prev = sets[sets.length - 1].interval_vector_unordered
    let iv = []
    for (let i = 0; i < iv_count; i++) iv[i] = prev[i]
    table.push({OIV: iv, count: count})
    
    return table
}


const Set_Catalog = {} 
{
    const S = Set_Catalog

    let weighting = [0, 0, 5, 4, 3, 2, 1, -5, -4, -3, -2, -1]
    
    S.weight_IC7_order = true;
    S.sets = get_all_set_info()
    fill_interval_vectors(S.sets)
    fill_polarity_values(S.sets, translate_weighting_IC7_order_to_index_order(weighting))

    S.sets_to_display = S.sets
}


// todo: this is slow, (on chromium-based browser this is very slow!!!)
// seems like the slow part is the layout recalc part (or reflow)  
const draw_sets = () => {
    
    const S = Set_Catalog.sets_to_display
 
    const table   = document.createDocumentFragment()
    const columns = make_node("table")

    {
        const header = make_node("thead")
        const texts = ["Set", "Index", "UIV", "OIV", "PV"]
        for (let i = 0; i < 5; i++) {
            const h = make_node("th")
            h.textContent = texts[i]
            header.appendChild(h)
        }
        columns.appendChild(header)
    }
   
    // .replaceAll() is too new
    const replace = (s) => s.split(",").join(" ")

    const click_on_index = (i) => {
        const I = Set_Input 
        I.set = array_copy(Set_Catalog.sets_to_display[i].data)
        I.which_form = 1 // reset state
        update_input()
    }

    for (let i = 0; i < S.length; i++) {
        
        const data = S[i]
        
        const row   = make_node("tr")
        const set   = make_node("td")
        const index = make_node("td")
        const UIV   = make_node("td")
        const OIV   = make_node("td")
        const PV    = make_node("td")
        
        set  .textContent = "{ " + replace(data.data.toString()) + " }"
        index.textContent = data.index.toString()
        UIV  .textContent = replace(data.interval_vector_unordered.toString())
        OIV  .textContent = replace(data.interval_vector_ordered.toString())
        PV   .textContent = data.polarity_value.toString()
        
        set.className = "setCopy"
        set.onclick = () => click_on_index(i)
       
        row.appendChild(set)
        row.appendChild(index)
        row.appendChild(UIV)
        row.appendChild(OIV)
        row.appendChild(PV)

        columns.appendChild(row)
    }

    table.appendChild(columns)
    
    {
        const div = get_node("setTable")
        div.removeChild(div.querySelector("table"))
        div.appendChild(table)
    }
}



/* ==== Set Input ==== */

const Set_Input = {
    set: [],
    which_form: 1, // todo: manage this state is pretty ugly, find a better way
}




/* === Clock Diagram === */

// todo: draw tonnetz and analytics 

const ClockDiagram = {}
{
    const C = ClockDiagram
    const svg = get_node("display").appendChild(make_svg_node("svg"))
    C.handle     = svg
    C.main_ring  = svg.appendChild(make_svg_node("circle"))
    C.paddings   = svg.appendChild(make_svg_node("g"))
    C.notes      = svg.appendChild(make_svg_node("g"))
    C.note_rings = svg.appendChild(make_svg_node("g"))
}

// todo: cleanup 
const draw_clock_diagram = () => {

    const set = (node, attrib, value) => node.setAttribute(attrib, value)
    
    const svg        = ClockDiagram.handle     
    const main_ring  = ClockDiagram.main_ring  
    const paddings   = ClockDiagram.paddings   
    const notes      = ClockDiagram.notes      
    const note_rings = ClockDiagram.note_rings 
    
    const svg_radius = 175
    const radius     = 150
    const svg_size   = svg_radius * 2

    set(svg, "id"    , 0)
    set(svg, "class" , "clockDiagram")
    set(svg, "width" , svg_size)
    set(svg, "height", svg_size)
    
    set(main_ring, "id"          , "main_ring")
    set(main_ring, "fill"        , "#1d2831")
    set(main_ring, "stroke-width", 6)
    set(main_ring, "stroke"      , "#1ab188")
    set(main_ring, "cx"          , svg_radius)
    set(main_ring, "cy"          , svg_radius)
    set(main_ring, "r"           , radius)
    
    set(paddings, "id", "paddings")
    for (let i = 0; i < 12; i++) {
        let p = paddings.appendChild(make_svg_node("circle"))
        set(p, "id"       , i)
        set(p, "fill"     , "#1ab188")
        set(p, "cx"       , svg_radius)
        set(p, "cy"       , svg_radius)
        set(p, "r"        , 22)
        set(p, "transform", ("rotate(" + 30 * i + "," + svg_radius + "," + svg_radius + ") translate(0, -" + radius + ")"))
    }

    set(notes, "id", "notes")
    {
        const get_pos_with_offset = (n, i, x, y) => {
            set(n, "x", svg_radius + Math.sin(Math.PI * (i / 6)) * radius + x)
            set(n, "y", svg_radius - Math.cos(Math.PI * (i / 6)) * radius + y)
        }

        for (let i = 0; i < 12; i++) {
            
            let n = notes.appendChild(make_svg_node("text"))
            set(n, "id", i)
            set(n, "fill", "#fff")
            n.textContent = i
            
            if (i < 10) get_pos_with_offset(n, i,  -6, 7)
            else        get_pos_with_offset(n, i, -12, 7)
        }
    }
    
    set(note_rings, "id", "noteRings")
    for (let i = 0; i < 12; i++) {
        let nr = note_rings.appendChild(make_svg_node("circle"))
        set(nr, "id"       , i)
        set(nr, "cx"       , svg_radius)
        set(nr, "cy"       , svg_radius)
        set(nr, "r"        , 22)
        set(nr, "transform", ("rotate(" + 30 * i + "," + svg_radius + "," + svg_radius + ") translate(0, -" + radius + ")"))
    }
    
    // make inputs
    {
        let note_rings = ClockDiagram.note_rings.querySelectorAll("circle")
        const I = Set_Input

        const click_on_index = (i) => {
            if (I.set.includes(i)) {
                I.set.splice(I.set.indexOf(i), 1)
            } else {
                I.set.push(i)
            }
            update_input()
        }
        
        for (let i = 0; i < 12; i++) {
            note_rings[i].onclick = () => click_on_index(i)
        }
    }
}

// todo: cleanup  
const update_clock_diagram = () => {

    const paddings    = ClockDiagram.paddings.querySelectorAll("circle")
    const note_values = ClockDiagram.notes   .querySelectorAll("text")
    
    for (let i = 0; i < 12; i++) {
        paddings   [i].setAttribute("fill", "#fff")
        note_values[i].setAttribute("fill", "#24313c")
    }
    
    for (let n of Set_Input.set) {
        paddings   [n].setAttribute("fill", "#1ab188")
        note_values[n].setAttribute("fill", "#fff")
    }
}





/* ==== Audio ==== */

const AudioInfo = {
    context: new AudioContext
}

// todo: figure out how exactly .setValueAtTime() works  
const play_set = (input, speed = 4) => {
    
    const A = AudioInfo
    const C = A.context 
    
    if (!speed) speed = 4
    
    C.resume()
    
    const osc  = C.createOscillator()
    const gain = C.createGain()
    osc.type = "sawtooth"
    osc.connect(gain)
    osc.start()
   
    // todo: swap oscillator, is this slow, or bad?
    if (A.oscillator) A.oscillator.stop(0) 
    gain.gain.setValueAtTime(0, C.currentTime)
    gain.connect(C.destination)
    A.oscillator = osc 

    const play_note = (p, t0, t1) => {
        const volume = 0.3
        const now    = C.currentTime
        osc.detune.setValueAtTime((p - 9) * 100, now + t0)
        gain.gain .setValueAtTime(volume,        now + t0)
        gain.gain .setValueAtTime(0,             now + t1)
    }
    
    for (let i = 0; i < input.length; i++) { 
        play_note(input[i], i / speed, (i + 1) / speed)
    }
}





/* ==== Set Transform ==== */

// todo: handle inputs like {0, 5, 7} and 0,5,7
const parse_input = () => {
    
    const input = get_node("setInput")
    let set = []

    let values = input.value.split(/\s+/)
    for (let s of values) {
        let v = parseInt(s, 10) 
        if (v < 12 && !isNaN(v)) set.push(v)
    }

    set = Array.from(new Set(set)) // get rid of repeated values, ehh...
    Set_Input.set = set
}

// Transpose by given amount
const transpose_set = (x) => {
    const I = Set_Input
    if (Number.isInteger(x)) {
        for (let i = 0; i < I.set.length; i++) { 
            I.set[i] = mod(I.set[i] + x, 12)
        }
    }
}

// Shift index of all by to given amount
const shift_index = (x) => {
    if (Number.isInteger(x)) { 
        let buffer = array_copy(Set_Input.set)
        let l = buffer.length
        for (let i = 0; i < l; i++) {
            Set_Input.set[i] = buffer[mod(i + x, l)]
        }
    }
}





/* ==== GUI ==== */

// after updating Set_Input, use this
// todo: maybe this is a bad way to do this
const update_input = () => {
    
    const input = get_node("setInput")
    const set   = Set_Input.set
    
    // Write Set_Input to input with whitespace
    input.value = ""
    if (set[0] !== undefined) {
        for (let n of set) input.value += n + " "
        input.value = input.value.slice(0, -1)
    }
    
    update_clock_diagram()
}

get_node("setInput").oninput = () => {
    Set_Input.which_form = 1 // reset state
    parse_input()
    update_clock_diagram()
}

const UI_play = () => play_set(Set_Input.set, parseFloat(get_node("speed").value)) // todo: bad input?

const UI_normalize = () => {
    transpose_set(-Set_Input.set[0])
    update_input()
}

const UI_sort = () => {
    const I = Set_Input
    I.which_form = 1 // reset state
    I.set.sort((a, b) => a - b)
    update_input()
}

const UI_mirror = () => {
    const I = Set_Input
    I.which_form = 1 // reset state
    for (let i = 0; i < I.set.length; i++) {
        I.set[i] = (12 - I.set[i]) % 12
    }
    I.set.sort((a, b) => a - b)
    update_input()
}

const UI_transposeUp = () => {
    transpose_set(1)
    update_input()
}

const UI_transposeDown = () => {
    transpose_set(-1)
    update_input()
}

const UI_indexUp = () => {
    shift_index(1)
    update_input()
}

const UI_indexDown = () => {
    shift_index(-1)
    update_input()
}

const UI_nextMode = () => {

    const I = Set_Input
    I.which_form = 1 // reset state
    I.set.sort((a, b) => a - b)

    shift_index(1)
    transpose_set(-I.set[0])
    update_input()
}

// todo: validate this
// todo: manage which_form state is pretty ugly, find a better way
const UI_nextForm = () => {
    
    const I = Set_Input
    const l = I.set.length
    if (l === 0) return
    I.set.sort((a, b) => a - b)
    
    let x = I.which_form

    x += 1
    x = mod(x, l)

    while (x === 0 || gcd(l, x) !== 1) {
        x += 1 
        x = mod(x, l)
    }
    
    I.which_form = x
   
    let buffer = array_copy(I.set)
    for (let i = 0; i < l; i++) {
        I.set[i] = buffer[mod(i * x, l)]
    }

    update_input()
}

get_node("play")         .onclick = UI_play
get_node("normalize")    .onclick = UI_normalize
get_node("sort")         .onclick = UI_sort
get_node("mirror")       .onclick = UI_mirror
get_node("transposeUp")  .onclick = UI_transposeUp
get_node("transposeDown").onclick = UI_transposeDown
get_node("indexUp")      .onclick = UI_indexUp
get_node("indexDown")    .onclick = UI_indexDown
get_node("nextMode")     .onclick = UI_nextMode
get_node("nextForm")     .onclick = UI_nextForm





/* ---- Shortcuts ---- */

document.onkeyup = (e) => {
    if (e.key === " ") e.preventDefault() // prevent spacebar act as click
}

document.onkeypress = (e) => {
    
    if (e.target.nodeName === "INPUT") return // todo: nodeName or localName? understand this

    switch (e.key) {    

        case " ": { 
            e.preventDefault() // prevent spacebar act as scroll
            UI_play()
            break
        }
        
        case "q": UI_sort(); break
        case "w": UI_mirror(); break
        case "e": UI_normalize(); break
        case "a": UI_nextMode(); break
        case "s": UI_nextForm(); break
        default: break
    }
}

document.onkeydown = (e) => {

    if (e.target.nodeName === "INPUT") return // todo: nodeName or localName? understand this

    switch (e.key) {    

        case "ArrowUp":    {
            e.preventDefault() // prevent scroll
            UI_transposeUp()   
            break
        }
        
        case "ArrowDown":  {
            e.preventDefault() // prevent scroll
            UI_transposeDown()
            break
        }

        case "ArrowLeft":  {
            e.preventDefault() // prevent scroll
            UI_indexDown()     
            break
        }

        case "ArrowRight": {
            e.preventDefault() // prevent scroll
            UI_indexUp()
            break
        }

        default: break
    }
}





/* ---- Right ---- */

get_node("allSets").onclick = () => {
    const S = Set_Catalog
    S.sets.sort(Compares.index_ascend)
    S.sets_to_display = S.sets
    draw_sets()
}

get_node("isSparse").onclick = () => {
    const S = Set_Catalog
    S.sets_to_display = S.sets_to_display.filter(Set_Filters.is_sparse)
    draw_sets()
}

get_node("isNeutral").onclick = () => {
    const S = Set_Catalog
    S.sets_to_display = S.sets_to_display.filter(Set_Filters.is_neutral)
    draw_sets()
}

get_node("pureTertian").onclick = () => {
    const S = Set_Catalog
    S.sets_to_display = S.sets_to_display.filter(Set_Filters.is_pure_tertian)
    draw_sets()
}

get_node("sizeInput").onkeypress = (e) => {
    
    if (!(e.key === "Enter")) return
    
    const S = Set_Catalog
    
    let text = get_node("sizeInput").value
    if (text === "") {
        S.sets_to_display = S.sets
        draw_sets()
        return
    }
    
    let value = parseInt(text, 10)
    if (value > 12 || value < 0 || isNaN(value)) return

    S.sets_to_display = S.sets_to_display.filter(Set_Filters.by_size(value))
    draw_sets()
}

get_node("pvInput").onkeypress = (e) => {
    
    if (!(e.key === "Enter")) return
    
    const S = Set_Catalog
    
    let text = get_node("pvInput").value
    if (text === "") {
        S.sets_to_display = S.sets
        draw_sets()
        return
    }
    
    let value = parseInt(text, 10)
    if (isNaN(value)) return

    S.sets_to_display = S.sets_to_display.filter(Set_Filters.by_PV(value))
    draw_sets()
}


get_node("uivInput").onkeypress = (e) => {
    
    if (!(e.key === "Enter")) return
    
    const S = Set_Catalog
    
    let text = get_node("uivInput").value
    if (text === "") {
        S.sets_to_display = S.sets
        draw_sets()
        return
    }
    
    let iv = []
    let values = text.split(/\s+/)
    
    for (let i = 0; i < 6; i++) {
        let v = parseInt(values[i], 10) 
        if (v > 6 || v < 0 || isNaN(v)) break 
        iv.push(v)
    }

    if (iv.length !== 6) return

    S.sets_to_display = S.sets_to_display.filter(Set_Filters.by_UIV(iv))
    
    draw_sets()
}

get_node("weightInput").onkeypress = (e) => {
    
    if (!(e.key === "Enter")) return

    const S = Set_Catalog
    
    let text = get_node("weightInput").value
    
    let weighting = []
    let values = text.split(/\s+/)
    
    for (let i = 0; i < 12; i++) {
        let v = parseInt(values[i], 10) 
        if (isNaN(v)) break 
        weighting.push(v)
    }

    if (weighting.length !== 12) return
    if (S.weight_IC7_order) weighting = translate_weighting_IC7_order_to_index_order(weighting) 
    fill_polarity_values(S.sets, weighting)

    draw_sets()
}

get_node("weightOrder").onclick = () => {
   
    const S = Set_Catalog
    const node = get_node("weightOrder")
    
    if (S.weight_IC7_order) node.textContent = "Index"
    else                    node.textContent = "IC7"
    
    S.weight_IC7_order = !S.weight_IC7_order
}


// todo: after clicking these, find a way to hide the panel
// or, maybe the behaviour now is actually useful?

get_node("sortByIndex").onclick = () => {
    const S = Set_Catalog
    S.sets_to_display.sort(Compares.index_ascend)
    draw_sets()
}

get_node("sortBySize").onclick = () => {
    const S = Set_Catalog
    S.sets_to_display.sort(Compares.count_ascend)
    draw_sets()
}

get_node("sortByUIV").onclick = () => {
    const S = Set_Catalog
    S.sets_to_display.sort(Compares.UIV_ascend)
    draw_sets()
}

get_node("sortByOIV").onclick = () => {
    const S = Set_Catalog
    S.sets_to_display.sort(Compares.OIV_ascend)
    draw_sets()
}

get_node("sortByPV").onclick = () => {
    const S = Set_Catalog
    S.sets_to_display.sort(Compares.PV_descend)
    draw_sets()
}




/* ==== Main ==== */

draw_clock_diagram()
draw_sets()
parse_input()
update_clock_diagram()


